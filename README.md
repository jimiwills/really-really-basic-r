# Really, really basic R

This project is about introducing biological researchers to R so that 
they get more out of any subsequent data processing workshops they 
might attend that use R.

The files in this project are released under the BSD license.

## This file - a Markdown file

This file is a "Markdown" file, which means that it looks fine in a
very simple text editor, but can also have some helpful highlighting
applied if view in a more advanced editor, like in R Studio.

In Markdown, section titles have one or more hashes (#) at the start
of the line.  That might be the only feature of Markdown I use in this
file!  Nope, I'm also using lists (lines start with a hyphen) and 
emphasis (words with asterisks fore and aft)

This file contains some descriptive information about R and RStudio,
but no R code/commands.

## The other file - an R file

There is another file in this project called really-really-basic-r.R
That file is an R script that you can read after you've read this 
README.  That file contains code (and helpful comments) that you can
work through.

## What is R?

R is an interpretted statistical language, written by statistician
for statisticians.  R is also the name given to the program that
implements R.

### R, the language

If you've done any programming before, you may find R quite different.
It focusses on vectors and matrices of data, and functions that process
a whole bunch of data at once.  Other languages use a lot of looping
structures to cycle through data and process it value by value, but in
R looping structures are very rarely used.

If you've never done any programming before, but you've done some
statistics, then you'll find R fairly intuitive as its raison d'etre is
performing statistics on data, and the way the language represents things
is much more pallatable to a statistician than many other languages.

If you're not a programmer or a statistician, don't worry.  R is a
lovely gentle way to learn about both... once you get over the initial
learning curve.  Hence this workshop!

### R the interpreter

R is a program that interprets the R language and performs actions on
the computer based on the commands given.  R is command based, and commands
can be entered on the R console.  Each time you enter a command, it is 
run immediately.  However, R commands can be groups together in a file
called an R script (e.g. really-really-basic-r.R) that makes it easier to
run the same (or optionally most of the same) commands in the future.  

## RStudio

RStudio is what's known as an IDE - and integrated development environment.
RStudio makes is much easier to work with R in a number of ways:

- R console that you can type commands into directly
- Editor that can edit R scripts, markdown files, etc
- Environment explorer that shows you what data is currently loaded
- Data viewer that can present data in tabular format for manual inspection
- Command history
- Interface to version control systems/repositories
- File explorer
- Package manager to install extra functionality
- Separate displays for help, plots, other stuff.

There are other IDEs too, but I like RStudio so that's what we'll use.

## What now?

If you don't already have them, install R and RStudio.

[R](https://cran.r-project.org/)
[RStudio](https://www.rstudio.com/)

## What next?

Load up really-really-basic-r.R in R studio and continue.

## What is not covered here

We will not be installing any additional packages, nor discussing
the best/worst way of doing this, that or the other.  The is a really,
really basic intro to R and will focus on *understanding* the basics.

## Data in R

Different types of data can be stored in R, and they can be stored in different ways.

If you work with spreadsheets, the most familiar type of data storage might be the *data.frame*.  
This type of data structure has a number of named columns, all of which are the same length, but 
they can be any type of data.  The types of data are *logical* (true/false), *numeric*, *integer*, *complex*,
*character* (alphanumeric, or perhaps binary?) and *raw* (numerical representation of characters).  

Each column in the *data.frame* is a *vector*.  A *vector* has one type of data in it.  In fact, a *vector*
is a structure in its own right, and is one of the most used structures in R.  R does not really have
single variables, like other languages, and a *vector* is the simplest structure you can get.  All Rs
basic functionality works on *vectors*, so if you want to take logs of a column, for example, you can
(do this for the whole column in one very simple command, and you do not need to do it for each 
value separately.  This is a fantastic feature of R, and one that distinguished it from most other
langauges.

If you have a number of columns that are all the same type, these could equally be represented in 
a *matrix*.  A *matrix* is a lot like a *data.frame*, except that the whole data structure has only one type.
In fact, it's basically a *vector* with some extra information about dimensions, since a *matrix* has a width
as well as a length.  Expanding on this, you can also have an *array* of n-dimensions, simply by specifying
the sizes of the dimensions, e.g. a cube of data would have with, length and height (or something like that)

A very different type of data structure is a *list*.  It's only a generic (untyped) vector, but this means
that each member can be any type of data using any datastructure.  It is a good way to group related items
or represent complex data that do not fit into the other data structures.

## Other stuff in R

R is command driven, and each command does something in some way.  You can call functions, most of which 
require you to specify parameters (such as the data to work on) and return some value.  You can use punctuation
that represents addition, subtraction, etc.  You can compare values.  You can assign a value to a variable.
You can use control structures to loop over data (if absolutely necessary) or run commands only if certain
conditions are met.  

There are functions in R that allow reading/writing data from/to files, transforming data, imputing data,
statistical summary, plotting, etc, etc, etc.

## Other places to learn...

http://www.tutorialspoint.com/r/
http://tryr.codeschool.com/
http://www.r-tutor.com/





